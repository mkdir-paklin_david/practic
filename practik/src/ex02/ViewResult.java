package ex02;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import ex01.Item2d;

public class ViewResult implements View {
	private static final String FNAME = "Item2d.bin";
	private static final int DEFAULT_NUM = 4;
	private ArrayList<Item2d> items = new ArrayList<Item2d>();
	public ViewResult() {
		this(DEFAULT_NUM);
	}
	public ViewResult(int n) {
		for(int ctr = 0; ctr < n; ctr++) {
			items.add(new Item2d());
		}
	}
	public ArrayList<Item2d> getItems() {
		return items;
	}

	public int calcPotentialEnergy(int Mass, int Height) {
		return (int) (Mass * 10 * Height);
	}

	public void init(int stepMass, int stepHeight) {
		int Mass = 0;
		int Height = 0;
		for (Item2d item : items) {
			item.setdata(Mass, Height);
			Mass += stepMass;
			Height += stepHeight;
		}
	}

	public void viewInit() {
		int randomInt = (int) (Math.random() * 30);
		int randomHeight = (int) (Math.random() * 30);
		init(randomInt, randomHeight);
	}

	public void viewSave() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(items);
		os.flush();
		os.close();
	}

	public void viewRestore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		items = (ArrayList<Item2d>) is.readObject();
		is.close();
	}
	public void viewHeader() {
		System.out.println("Results:");
	}
	public void viewBody() {
		for(Item2d item : items) {
			int energy = calcPotentialEnergy(item.getMass(), item.getHeight());
			String binary1 = Integer.toBinaryString(energy);
			int num = Integer.parseInt(binary1, 2);
			int maxSequence = 0;
			int currentSequence = 0;
			while (num != 0) {
				if (num % 2 == 1) {
					currentSequence++;
				} else {         
					if (currentSequence > maxSequence) {
						maxSequence = currentSequence;
					}
					currentSequence = 0; 
				}
				num /= 2; 
			}   
			if (currentSequence > maxSequence) {
				maxSequence = currentSequence;
			}

			System.out.printf("(Mass: %d; Height: %d; -> ", item.getMass(), item.getHeight());
			System.out.println("Potential energy: " + energy + ") " + Integer.toBinaryString(energy) + " Maximum sequence: " + maxSequence);
		}
	}
	public void viewFooter() {
		System.out.println("End.");
	}
	public void viewShow() {
		viewHeader();
		viewBody();
		viewFooter();
	}
}