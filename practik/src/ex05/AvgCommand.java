package ex05;
import java.util.concurrent.TimeUnit;
import ex01.Item2d;
import ex02.ViewResult;
import ex04.Command;

public class AvgCommand extends ViewResult implements Command {
	private double result = .0;
	private int progress = 0;
	private ViewResult viewResult;

	public ViewResult getViewResult() {
		return viewResult;
	}

	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}

	public AvgCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	public double getResult() {
		return result;
	}

	public boolean running() {
		return progress < 100;
	}

	@Override
	public void execute() {
		progress = 0;
		System.out.println("Average of the maximum sequence executed...");
		result = 0;
		int idx = 1, size = viewResult.getItems().size();
		for (Item2d item : viewResult.getItems()) {
			int energy = calcPotentialEnergy(item.getMass(), item.getHeight());
			String binary1 = Integer.toBinaryString(energy);
			int num = Integer.parseInt(binary1, 2);
			int maxSequence = 0;
			int currentSequence = 0;
			while (num != 0) {
				if (num % 2 == 1) {
					currentSequence++;
				} else {         
					if (currentSequence > maxSequence) {
						maxSequence = currentSequence;
					}
					currentSequence = 0; 
				}
				num /= 2; 
			}   
			if (currentSequence > maxSequence) {
				maxSequence = currentSequence;
			}
			result += maxSequence;
			progress = idx * 100 / size;

			if (idx++ % (size / 2) == 0) {
				System.out.println("Average " + progress + "%");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(2000 / size);
			} catch (InterruptedException e) {
				System.err.println(e);
			}
		}
		result /= size;
		System.out.println("Average of the maximum sequence done. Result = " + String.format("%.2f",result));
		progress = 100;
	}
}