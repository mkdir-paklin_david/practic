package ex05;

import java.util.concurrent.TimeUnit;

import ex01.Item2d;
import ex02.ViewResult;
import ex04.Command;
import java.util.Arrays;

public class MaxCommand extends ViewResult implements Command {
	private int result = -1;
	private int progress = 0;
	private ViewResult viewResult;
	public ViewResult getViewResult() {
		return viewResult;
	}
	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}
	public MaxCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}
	public int getResult() {
		return result;
	}
	public boolean running() {
		return progress < 100;
	}
	@Override
	public void execute() {
		progress = 0;
		System.out.println("Max executed...");
		int size = viewResult.getItems().size();
		result = 0;
		int idx = 0;
		int resultMax = -1;
		for (Item2d item : viewResult.getItems()) {
			if (viewResult.getItems().get(result).getHeight() <
					viewResult.getItems().get(idx).getHeight()) {
				result = idx;
			}
			progress = idx * 100 / size;
			idx++;
			if (idx % (size / 3) == 0) {
				System.out.println("Max " + progress + "%");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(3000 / size);
			} catch (InterruptedException e) {
				System.err.println(e);
			}
			int energy = calcPotentialEnergy(item.getMass(), item.getHeight());
			String binary1 = Integer.toBinaryString(energy);
			int num = Integer.parseInt(binary1, 2);
			int maxSequence = 0;
			int currentSequence = 0;
			int[] sequenceArray = new int[binary1.length()];
			int i = 0;
			while (num != 0) {
				if (num % 2 == 1) {
					currentSequence++;
				} else {
					sequenceArray[i] = currentSequence;
					i++;
					if (currentSequence > maxSequence) {
						maxSequence = currentSequence;
					}
					currentSequence = 0;
				}
				num /= 2;
			}
			if (currentSequence > maxSequence) {
				maxSequence = currentSequence;
			}
			sequenceArray[i] = currentSequence; 
			int currentMax = Arrays.stream(sequenceArray).max().orElse(-1); 
			if (currentMax > resultMax) {
	            resultMax = currentMax;
	        }
		}
		System.out.println("Max done. Item #" + result +
				" found: " + resultMax);
		progress = 100;
	}
}