package ex03;
import java.util.Formatter;
import ex01.Item2d;
import ex02.ViewResult;

public class ViewTable extends ViewResult {
	private static final int DEFAULT_WIDTH = 66;
	private int width;
	public ViewTable() {
		width = DEFAULT_WIDTH;
	}
	public ViewTable(int width) {
		this.width = width;
	}

	public ViewTable(int width, int n) {
		super(n);
		this.width = width;
	}

	public int setWidth(int width) {
		return this.width = width;
	}

	public int getWidth() {
		return width;
	}

	private void outLine() {
		for(int i = width; i > 0; i--) {
			System.out.print('-');
		}
	}

	private void outLineLn() {
		outLine();
		System.out.println();
	}

	private void outHeader() {
	    Formatter fmt = new Formatter();
	    fmt.format("%-10s | %-10s | %-10s | %-10s | %-10s\n", "    Mass", "  Height", "  Energy", " Sequence", "  Binary");
	    System.out.print(fmt.toString());
	}

	public void outBody() {
	    Formatter fmt = new Formatter();
	    fmt.format("%-10d | %-10d | %-10d | %-10d | %-10d\n", 0, 0, 0, 0, 0);
	    for(Item2d item : getItems()) {
	        int energy2 = calcPotentialEnergy(item.getMass(), item.getHeight());
	        String binary1 = Integer.toBinaryString(energy2);
			int num = Integer.parseInt(binary1, 2);
			int maxSequence = 0;
			int currentSequence = 0;
			while (num != 0) {
				if (num % 2 == 1) {
					currentSequence++;
				} else {         
					if (currentSequence > maxSequence) {
						maxSequence = currentSequence;
					}
					currentSequence = 0; 
				}
				num /= 2; 
			}   
			if (currentSequence > maxSequence) {
				maxSequence = currentSequence;
			}
	        System.out.printf("%-10d | %-10d | %-10d | %-10d | %-10s\n", item.getMass(), item.getHeight(), energy2, maxSequence, binary1);
	    }
	}

	public final void init(int width) { 
		this.width = width;
		viewInit();
	}
	public final void init(int width, int stepMass, int stepHeight) { 
		this.width = width;
		init(stepMass, stepHeight);
	}
	@Override
	public void init(int stepMass, int stepHeight) {
		System.out.print("Initialization... ");
		super.init(stepMass, stepHeight);
		System.out.println("done. ");
	}
	@Override
	public void viewHeader() {
		outHeader();
		outLineLn();
	}
	@Override
	public void viewBody() {
		outBody();
	}
	@Override
	public void viewFooter() {
		outLineLn();
	}
}