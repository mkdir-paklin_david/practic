package ex01;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Calc {
	private static final String FNAME = "Item2d.bin";
	private Item2d result;
	public Calc() {
		result = new Item2d();
	}

	public void setResult(Item2d result) {
		this.result = result;
	}

	public Item2d getResult() {
		return result;
	}

	private int calcPotentialEnergy(int Mass, int Height) {
		return (int) (Mass * 10 * Height);
	}

	public void init(int Mass, int Height) {
		int triangleArea = calcPotentialEnergy(Mass, Height);
		int decimal1 = triangleArea;
		result.setMass(Mass);
		result.setHeight(Height);
		String binary1 = Integer.toBinaryString(decimal1);
		int num = Integer.parseInt(binary1, 2);
		int maxSequence = 0;
		int currentSequence = 0;
		while (num != 0) {
			if (num % 2 == 1) {
				currentSequence++;
			} else {         
				if (currentSequence > maxSequence) {
					maxSequence = currentSequence;
				}
				currentSequence = 0; 
			}
			num /= 2; 
		}   
		if (currentSequence > maxSequence) {
			maxSequence = currentSequence;
		}
		System.out.println("Maximum sequence of units: " + maxSequence);
		System.out.println("In decimal number system potential energy: " + triangleArea);
		System.out.println("In binary number system potential energy: " + binary1);
	}

	public void show() {
		System.out.println("Input data: " + result);
	}

	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(result);
		os.flush();
		os.close();
	}

	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		result = (Item2d)is.readObject();
		is.close();
	}
}