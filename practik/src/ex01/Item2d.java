package ex01;

import java.io.Serializable;

public class Item2d implements Serializable {
	private int Mass;
	private int Height;
	private static final long serialVersionUID = 1L;

	public Item2d() {
		Mass = 0;
		Height = 0;
	}

	public Item2d(int Mass, int Height) {
		this.Mass = Mass;
		this.Height = Height;
	}

	public int setMass(int Mass) {
		return this.Mass = Mass;
	}

	public int getMass() {
		return Mass;
	}

	public int setHeight(int Height) {
		return this.Height = Height;
	}

	public int getHeight() {
		return Height;
	}

	public Item2d setdata(int Mass, int Height) {
		this.Mass = Mass;
		this.Height = Height;
		return this;
	}

	public String toString() {
		return "Mass = " + Mass + ", Height = " + Height;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item2d other = (Item2d) obj;
		if (Integer.toBinaryString(Mass) != Integer.toBinaryString(other.Mass))
			return false;
		if (Math.abs(Math.abs(Height) - Math.abs(other.Height)) > .1e-10)
			return false;
		return true;
	}
}