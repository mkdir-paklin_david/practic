package ex04;
import ex01.Item2d;

public class ChangeItemCommand implements Command {

	private Item2d item;

	private int offset;

	public Item2d setItem(Item2d item) {
		return this.item = item;
	}

	public Item2d getItem() {
		return item;
	}

	public int setOffset(int offset) {
		return this.offset = offset;
	}

	public int getOffset() {
		return offset;
	}
	@Override
	public void execute() {
		item.setMass(item.getMass() * offset);
		item.setHeight(item.getHeight() * offset);
	}
}